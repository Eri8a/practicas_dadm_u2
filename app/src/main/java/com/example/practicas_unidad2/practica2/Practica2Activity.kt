package com.example.practicas_unidad2.practica2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica2.*

const val DURATION_RESULT = 3000

class Practica2Activity : AppCompatActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK){
            when(requestCode){
                DURATION_RESULT ->{
                    if (data != null){
                        val minutes = data.getIntExtra("DURATION", -1)
                        tvSelected.text = "$minutes minutes"
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica2)

        btnSelector.setOnClickListener {
            val intent = Intent(this, DurationSelectorActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)
        }

        }


    }
