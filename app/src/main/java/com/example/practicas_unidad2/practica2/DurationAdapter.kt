package com.example.practicas_unidad2.practica2

import android.net.sip.SipSession
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_unidad2.R
import kotlinx.android.synthetic.main.item_list.view.*


class DurationAdapter (private val listener: (Int) -> Unit): RecyclerView.Adapter<DurationAdapterViewHolder>(){

    private var list = mutableListOf<Int>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DurationAdapterViewHolder {
        val intemView = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return DurationAdapterViewHolder(intemView)
    }

    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int) {
      holder.setData(list[position], listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }
    fun setList(list: List<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
 class DurationAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

fun setData (duration: Int, listener: (Int) -> Unit){
    itemView.apply {
        var horas: Int = 0
        if (duration / 60 > 0) {
            horas = duration / 60
            tvMinutes.text = context.resources.getQuantityString(R.plurals.pluralsHours, horas, horas)
        } else {
            tvMinutes.text = "$duration minutes"
        }
        setOnClickListener { listener.invoke(duration) }

    }
}
        }
