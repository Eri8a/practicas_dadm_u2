package com.example.practicas_unidad2.Practica3

enum class Gender (var namee: String){
    MALE("Hombre"),
    FEMALE("Mujer"),
    NOT_BINARY("No binario"),
    NOT_IDENTYFIED("Sin especificar")
}
