package com.example.practicas_unidad2.pract

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony
import android.telephony.SmsManager
import android.view.View
import androidx.core.app.ActivityCompat
import com.example.practicas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica1.*

class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)

            if (ActivityCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS) !=PackageManager.PERMISSION_GRANTED)
            {
             ActivityCompat.requestPermissions(this,
                     arrayOf(Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS), 101)
            }
        else
            receiveMsg()

           btnSMS.setOnClickListener{
               var sms : SmsManager = SmsManager.getDefault()
                sms.sendTextMessage(etPhone.text.toString(),"ME", etSMS.text.toString(),null, null)
           }
            }
        override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if (requestCode==101 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                receiveMsg()}


        private fun receiveMsg() {
        var br = object: BroadcastReceiver(){
            override fun onReceive(p0: Context?, p1: Intent?) {
                for (sms in Telephony.Sms.Intents.getMessagesFromIntent(p1)) {
                   etPhone.setText(sms.originatingAddress)
                   etSMS.setText(sms.displayMessageBody)
                }
            }
        }

        registerReceiver(br, IntentFilter("android.provider.Telephony.SMS_RECEIVED"))



        btnCall.setOnClickListener {
            val telephone = etPhone.text.toString()
            if (switchCM.isChecked)
            if (validateCallPermissions()){ //YA TENEMOS EL PERMISO---------------------------------
                val intent = Intent (Intent.ACTION_CALL, Uri.parse("tel:$telephone"))
                startActivity(intent)
            } else { //NO TENEMOS SISTEMA JOVEN-----------------------------------------------------
                val permissions = arrayOf(Manifest.permission.CALL_PHONE)
                ActivityCompat.requestPermissions(this, permissions, 101 )
            }
        }


    switchCM.setOnCheckedChangeListener { buttonView, isChecked ->
        if (isChecked) {
            switchCM.text = "Mensaje"
            etSMS.visibility = View.VISIBLE
            btnSMS.visibility = View.VISIBLE
            btnCall.text = "Enviar"
        } else {
            switchCM.text = "Llamada"
            etSMS.visibility = View.GONE
            btnSMS.visibility = View.GONE
            btnCall.text = "Llamar"
        }
    }
}

private fun validateCallPermissions(): Boolean {
        // val permissions = arrayOf(Manifest.permission.CALL_PHONE) ---> Para mas de un Permiso
      return ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE)  == PackageManager.PERMISSION_GRANTED
    }

    }

