package com.example.practicas_unidad2.Practica6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_unidad2.R
import com.example.practicas_unidad2.Practica6.CitiesAndTimezones.CitiesTimezones
import kotlinx.android.synthetic.main.item_city_timezone.view.*


class CitiesTimezonesAdapter(private val listener: (CitiesTimezones) -> Unit) :
    RecyclerView.Adapter<CitiesTimeZonesViewHolder>() {

    private var List = mutableListOf<CitiesTimezones>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesTimeZonesViewHolder {
        val itemview =
            LayoutInflater.from(parent.context).inflate(R.layout.item_city_timezone, parent, false)
        return CitiesTimeZonesViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: CitiesTimeZonesViewHolder, position: Int) {
        holder.setData(List[position], listener)
    }

    override fun getItemCount(): Int {
        return List.size
    }

    fun setList(list: List<CitiesTimezones>) {
        this.List.addAll(list)
        notifyDataSetChanged()
    }

}

class CitiesTimeZonesViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

    fun setData(item: CitiesTimezones, listener: (CitiesTimezones) -> Unit) {

        itemView.apply {
            tvCC.text = "${item.name}/${item.country}"
            tvTz.text = item.timeZoneName


            setOnClickListener { listener.invoke(item) }

        }

    }

}
