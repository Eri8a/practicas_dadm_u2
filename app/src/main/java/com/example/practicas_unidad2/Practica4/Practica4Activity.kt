package com.example.practicas_unidad2.Practica4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_unidad2.R
import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_practica4.*

class Practica4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4)

        btnAd.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("Esto es un cuadro de dialogo")
                .setTitle("HOLA")
                //.setCancelable(false) // Para evitar que ssalga del cuadro de dialogo
                .setPositiveButton("SIMON") { dialog, which ->
                    Toast.makeText(this, "Amonos Resio", Toast.LENGTH_SHORT).show()
                }
                .setNeutralButton("IDK") { dialog, which ->
                    Toast.makeText(this, "Meh", Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("NEL My Men") { dialog, which ->
                    Toast.makeText(this, "Uhh", Toast.LENGTH_SHORT).show()
                }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        btnAd2.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoje un color")
                .setItems(colors) { dialog, which ->
                    //which contiene el index del item seleccionado
                    when (which) {
                        0 -> Toast.makeText(this, "Red", Toast.LENGTH_SHORT).show()
                        1 -> Toast.makeText(this, "Blue", Toast.LENGTH_SHORT).show()
                        2 -> Toast.makeText(this, "Green", Toast.LENGTH_SHORT).show()
                        3 -> Toast.makeText(this, "Yellow", Toast.LENGTH_SHORT).show()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }

        btnAd3.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoje un color")
                .setSingleChoiceItems(colors, 0) { dialog, which ->
                    Toast.makeText(this, "Seleccionaste: ${colors[which]}", Toast.LENGTH_SHORT)
                        .show()
                }
            val dialog = builder.create()
            dialog.show()
        }

        val selectedItems = arrayListOf<Int>()
        btnAd4.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoje un color")
                .setMultiChoiceItems(colors, null, ) { dialog, which, isChecked ->
                    if (isChecked) {
                        //guardar indice
                        selectedItems.add(which)
                        Toast.makeText(this,
                            "Selected Items: ${selectedItems.size}",
                            Toast.LENGTH_SHORT).show()
                    } else if (selectedItems.contains(which)) {
                        //remover el indice
                        selectedItems.remove(which)
                        Toast.makeText(this,
                            "Selected Items: ${selectedItems.size}",
                            Toast.LENGTH_SHORT).show()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }

        btnAd5.setOnClickListener {
            Common.customDialog(this){ yes ->
                if (yes){
                    Toast.makeText(this, "YES", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "NEL", Toast.LENGTH_SHORT).show()
                }
            }.show()
        }
    }
}
