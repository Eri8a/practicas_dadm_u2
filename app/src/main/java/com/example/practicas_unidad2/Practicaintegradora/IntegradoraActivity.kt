package com.example.practicas_unidad2.Practicaintegradora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_unidad2.R
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_integradora.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter


class IntegradoraActivity : AppCompatActivity() {

        private val adapter by lazy {
            ClimaAdapter{ selectedClima ->

            }
        }

            override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_integradora)
                val ciudad = intent.getSerializableExtra("ciudad") as String
                Toast.makeText(this, ""+ciudad, Toast.LENGTH_SHORT).show()
                when (ciudad){
                    "Seoul" -> {

                    }
                }


                rvClima.adapter = adapter

                val input = resources.openRawResource(R.raw.weather)
                val writer = StringWriter()
                val buffer = CharArray(1024)
                input.use { input ->
                    val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
                    var n: Int
                    while (reader.read(buffer).also { n = it } != -1){
                        writer.write(buffer, 0, n)
                    }
                }
                val jsonObject = JSONObject(writer.toString())
                val tiempoList = Clima(jsonObject)

                adapter.setList(tiempoList.tiempo)

            }
                }

