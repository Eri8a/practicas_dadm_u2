package com.example.practicas_unidad2.Practicaintegradora

import org.json.JSONObject
import java.util.*


class Clima (jsonObject: JSONObject) {
    var timezone: String = ""
    var daily: ClimaTiempo
    var tiempo = arrayListOf<ClimaTiempo>()

    init {
        val dailyJo = jsonObject.getJSONObject("daily")
        daily = ClimaTiempo (dailyJo)
    }
    class ClimaTiempo(dailyJo: JSONObject){
        var time: Long = 0
        var icon = ""
        var temperatureMin: Double=0.0
        var temperatureMax: Double=0.0

        init {
            time = dailyJo.getLong("time")
            icon = dailyJo.getString("timeZoneName")
            temperatureMin = dailyJo.getDouble("temperatureMin")
            temperatureMax = dailyJo.getDouble("temperatureMax")
        }

    }
}
