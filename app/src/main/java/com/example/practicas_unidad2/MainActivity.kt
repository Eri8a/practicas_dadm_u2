package com.example.practicas_unidad2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_unidad2.Practica3.Practica3Activity
import com.example.practicas_unidad2.Practica4.Practica4Activity
import com.example.practicas_unidad2.Practica5.Practica5Activity
import com.example.practicas_unidad2.Practica6.Practica6Activity
import com.example.practicas_unidad2.Practicaintegradora.IntegradoraActivity
import com.example.practicas_unidad2.pract.Practica1Activity
import com.example.practicas_unidad2.practica2.Practica2Activity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPracticas1.setOnClickListener {
        val intent = Intent(this, Practica1Activity::class.java)
            startActivity(intent)
        }
        btnPracticas2.setOnClickListener {
            val intent = Intent(this, Practica2Activity::class.java)
            startActivity(intent)
        }
        btnPractica3.setOnClickListener {
            val intent = Intent(this, Practica3Activity::class.java)
            startActivity(intent)
        }
        btnPracticas4.setOnClickListener {
            val intent = Intent(this, Practica4Activity::class.java)
            startActivity(intent)
        }
        btnPractica5.setOnClickListener {
            val intent = Intent(this, Practica5Activity::class.java)
            startActivity(intent)
        }
        btnPractica6.setOnClickListener {
            val intent = Intent(this, Practica6Activity::class.java)
            startActivity(intent)
        }
        btnIntegradora.setOnClickListener {
            val intent = Intent(this, IntegradoraActivity::class.java)
            startActivity(intent)
        }
    }
    }





